package kz.halyk.rest.api.service;


import java.util.ArrayList;
import java.util.List;
import kz.halyk.rest.api.model.User;
import org.springframework.stereotype.Service;

/**
 * UserService.
 * */
@Service
public class UserService {

  /**
   * ListUser.
   * */
  public List<User> getUsers() {
    User user = new User();
    user.setName("Makhmud");
    user.setAge(18);

    List<User> users = new ArrayList<>();
    users.add(user);
    return users;
  }

  /**
   * Set user name.
   *
   * @param name of user.
   * */
  public User getUserByName(String name) {
    User user = new User();
    user.setName(name);
    user.setAge(18);

    return user;
  }

  //TODO: test is (using postman)
  /**
   * ListUser.
   * */
  public User insertUser(String name, int age) {
    User user = new User();
    user.setName(name);
    user.setAge(age);
    return user;
  }


}
