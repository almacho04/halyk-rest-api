package kz.halyk.rest.api.model;

import lombok.Data;

/**
 * User function.
 * */
@Data
public class User {
  String name;
  private int age;
}
