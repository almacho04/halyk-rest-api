package kz.halyk.rest.api;


import java.util.Collections;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Spring activate.
 * */
@SpringBootApplication
public class App {

  /**
   * Activation line.
   * */
  public static void main(String[] args) {
    SpringApplication app = new SpringApplication(App.class);
    app.setDefaultProperties(Collections
        .singletonMap("server.port", "8081"));
    app.run(args);
  }
}
