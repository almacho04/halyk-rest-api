package kz.halyk.rest.api.controller;


import java.util.List;
import kz.halyk.rest.api.model.User;
import kz.halyk.rest.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for users.
 * */
@RestController
public class UserController {

  @Autowired
  UserService userService;

  @SuppressWarnings("checkstyle:Indentation")
  @GetMapping("/users")
  public List<User> getUsers() {
    return userService.getUsers();
  }

  @GetMapping("/users/{name}")
  public User getUserByName(@PathVariable(name = "name") String name) {
    return userService.getUserByName(name);
  }

  @PostMapping("/users")
  public User insetUser(@RequestParam(name = "name") String name,
                        @RequestParam(name = "age") int age) {
    return userService.insertUser(name, age);
  }
}
